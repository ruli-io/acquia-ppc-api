'use strict'

const gulp = require('gulp')
const del = require('del')
const sass = require('gulp-sass')
const autoprefixer = require('gulp-autoprefixer')
const path = require('path')
const gutil = require('gulp-util')

const webpack = require('webpack')
let wpConf = require('./webpack.config.js')

var scriptsWatch = 'src/js/**/*'
var styles = [
    'src/scss/admin.scss'
]
var stylesWatch = 'src/scss/**/*'

gulp.task('default', [
    'scripts',
    'styles'
], function () {
    return true
})

gulp.task('dev', [
    'scripts',
    'styles',
    'scripts-watch',
    'styles-watch'
], function () {
    return true
})
// ToDo: update env to production before you launch
gulp.task('scripts', function (cb) {
    wpConf.devtool = '#source-map'
    wpConf.plugins = wpConf.plugins.concat([
        new webpack.DefinePlugin({
            'process.env': {
                NODE_ENV: JSON.stringify('development')
            }
        }),
        // new webpack.optimize.UglifyJsPlugin({
        //     sourceMap: true,
        //     compress: {
        //         warnings: false
        //     }
        // }),
        new webpack.LoaderOptionsPlugin({
            minimize: true
        })
    ])
    webpack(wpConf, function (err, stats) {
        if (err) {
            throw new gutil.PluginError('webpack', err)
        }
        gutil.log('[webpack]', stats.toString({
            // output options
        }))
        cb()
    })
})

gulp.task('scripts-watch', function () {
    gulp.watch(scriptsWatch, ['scripts'])
})

gulp.task('styles', function () {
    if (typeof sass !== 'undefined') {
        gulp.src(styles)
            .pipe(sass().on('error', sass.logError))
            .pipe(autoprefixer())
            .pipe(gulp.dest('public/assets/css'))
    }
})

gulp.task('styles-watch', function () {
    gulp.watch(stylesWatch, ['styles'])
})



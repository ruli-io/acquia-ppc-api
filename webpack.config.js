
const webpack = require('webpack')
const path = require('path')

var config = {
    cache: true,
    context: path.join(__dirname, 'src', 'js'),
    entry: {
        admin: './admin'
    },
    output: {
        path: path.join(__dirname, 'public', 'assets', 'js'),
        publicPath: '/assets/js/',
        filename: '[name].js'
    },
    module: {
        loaders: [
            {
                test: /\.vue$/,
                loader: 'vue-loader',
                options: {
                    loaders: {
                        css: [{
                            loader: 'style-loader!css-loader'
                        }],
                        js: [{
                            loader:'babel-loader'
                        }],
                        scss: [{
                            loader: 'style-loader'
                        }, {
                            loader: 'css-loader'
                        }, {
                            loader: 'sass-loader',
                            options: {
                                includePaths: ['./node_modules']
                            }
                        }]
                    }
                }
            },
            {
                test: /\.css$/,
                loader: 'style-loader!css-loader'
            },
            {
                test: /\.woff(2)?(\?v=[0-9]\.[0-9]\.[0-9])?$/,
                loader: "url-loader?limit=10000&mimetype=application/font-woff"
            },
            {
                test: /\.(ttf|eot|svg)(\?v=[0-9]\.[0-9]\.[0-9])?$/,
                loader: "file-loader"
            },
        ]
    },
    devServer: {
        historyApiFallback: true,
        noInfo: true
    },
    plugins: [
        new webpack.ProvidePlugin({
            '$': 'jquery'
        })
    ],
    resolve: {
        extensions: ['.js', '.vue'],
        alias: {
            'vue$': 'vue/dist/vue.esm.js'
        }
    },
    performance: {
        hints: false
    }
}

module.exports = config

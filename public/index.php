<?php
require __DIR__ . '/../vendor/autoload.php';

session_start();

// Instantiate the app
require __DIR__ . '/../src/config/constants.php';
$settings = require __DIR__ . '/../src/config/settings.php';
$app = new \Slim\App($settings);

// Set up dependencies
require __DIR__ . '/../src/config/dependencies.php';

// Register routes
require __DIR__ . '/../src/admin-routes.php';
require __DIR__ . '/../src/routes.php';

// Run app
$app->run();
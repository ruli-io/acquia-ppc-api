<?php
use Symfony\Component\Yaml\Yaml;
use ZionAndZion\Helper;
use ZionAndZion\Response;
use ZionAndZion\Ngram;

/**
 * Home Route - Documentation
 * //////////////////////////
 */
$app->get('/', function ($request, $response, $args) {
  include __DIR__ . '/../docs/v1.html';
});

/**
 * Image URL's Route
 * /////////////////
 */
$app->get('/images/{type}/{container}/{image_file}', function ($request, $response, $args) {

  if ($args['type'] === 'sections' && in_array($_SERVER['REMOTE_ADDR'], IP_WHITELIST)){
    $file_path = __DIR__ . '/../containers/' . $args['container'] . '/production/images/offers/_' . $args['type'] . '/' . $args['image_file'];
  } else {
    $file_path = __DIR__ . '/../containers/' . $args['container'] . '/production/images/' . $args['type'] . '/' . $args['image_file'];
  }

  // If file exists
  if (file_exists($file_path)) {
    $response->write(file_get_contents($file_path));
    return $response->withHeader('Content-Type', FILEINFO_MIME_TYPE);
  } else {
    return $response->withStatus(404)
      ->withJson(['response' => 'Image doesn\'t exist.']);
  }
});

/**
 * API ROUTE
 */
$app->get('/v1/{client}', function ($request, $response, $args) {
  $client = $args['client'];
  $token = $_GET['token'];
  $keywords = $_GET['keywords'];
  $clientPath = CONTAINERS_PATH . '/' . $client;
  $config = Yaml::parse(file_get_contents($clientPath . '/config.yml'));
  if (isset($config)) {
    $config['defaults']['terms'] = str_replace("\n", '<br/>', $config['defaults']['terms']);
  }

  /*
   * NO CONFIG DATA FOUND
   */
  if (!isset($config)) {
    $jsonResponse = array(
      'success' => false,
      'message' => 'No configuration found for ' . $client
    );

    return $response->withJson($jsonResponse, 400);
  }

  /*
   * REQUEST TOKEN DOES NOT MATCH CLIENT TOKEN
   */
  if ($config['token'] !== $token) {
    $jsonResponse = array(
      'success' => false,
      'message' => 'Invalid token for ' . $client
    );

    return $response->withJson($jsonResponse, 401);
  }

  /*
   * CLIENT DISABLED, RETURN DEFAULT INFO
   */
  if ($config['disabled'] === true) {
    $jsonResponse = array(
      'success' => true,
      'message' => 'Client currently disabled - Default data returned for ' . $client,
      'data' => $config['defaults']
    );

    return $response->withJson($jsonResponse, 200);
  }

  /*
   * NO KEYWORDS FOUND, RETURN DEFAULT INFO
   */
  if (!isset($keywords)) {
    $jsonResponse = array(
      'success' => true,
      'message' => 'No keywords provided - Default data returned for ' . $client,
      'data' => $config['defaults']
    );

    return $response->withJson($jsonResponse, 200);
  }

  $data = $config['defaults'];
  $search = new Ngram();
  $clientResponse = new Response($clientPath, 'production');
  $r = $clientResponse->getData('array');

  $offers = get_object_vars($r['offers']);
  $newOffer = $search->getBestMatch($keywords, array_keys($offers));

  if (isset($newOffer)) {
    $data['offer'] = $offers[$newOffer];
  }

  $banners = get_object_vars($r['banners']);
  $newBanner = $search->getBestMatch($keywords, array_keys($banners));

  if (isset($newBanner)) {
    $data['banner'] = $banners[$newBanner];
  }

  $headlines = get_object_vars($r['headlines']);
  $newHeadline = $search->getBestMatch($keywords, array_keys($headlines));
  if (isset($newHeadline)) {
    $data['headline'] = $headlines[$newHeadline];
  }

  $subheadlines = get_object_vars($r['subheadlines']);
  $newSubheadline = $search->getBestMatch($keywords, array_keys($subheadlines));
  if (isset($newSubheadline)) {
    $data['subheadline'] = $subheadlines[$newSubheadline];
  }

  $terms = get_object_vars($r['terms']);
  // find the term that matches the new offer
  $offersCsv = Helper::csvToData($clientPath . '/production/csv/offers.csv');
  $matchedTermKey = '';
  foreach($offersCsv as $keywords => $termKey){
    if ($keywords === $newOffer){
      $matchedTermKey = $termKey;
    }
  }

  $newTerm = $terms[$matchedTermKey];
  if (isset($newTerm)) {
    $data['terms'] = $newTerm;
  }

  $jsonResponse = array(
    'success' => true,
    'message' => 'Dynamic data returned for ' . $client,
    'data' => $data
  );
  return $response->withJson($jsonResponse, 200, JSON_UNESCAPED_SLASHES);
});

import vueRouter from 'vue-router'

import viewSections from './components/viewSections.vue'
import uploadCSVs from './components/uploadCSVs.vue'
import reviewAndApprove from './components/reviewAndApprove.vue'
import reviewHeadlines from './components/reviewHeadlines.vue'
import reviewSubHeadlines from './components/reviewSubHeadlines.vue'
import reviewBanners from './components/reviewBanners.vue'
import reviewOffers from './components/reviewOffers.vue'

const routes = [
    {
        path: '/',
        name: 'View Sections',
        component: viewSections
    },
    {
        path: '/upload-csvs',
        name: 'Upload CSVs',
        component: uploadCSVs
    },
    {
        path: '/review-and-approve',
        name: 'Review and Approve',
        component: reviewAndApprove,
        children:[
            {
                path: 'headlines',
                name: 'Headlines',
                component: reviewHeadlines
            },{
                path: 'sub-headlines',
                name: 'Sub Headlines',
                component: reviewSubHeadlines
            },{
                path: 'banners',
                name: 'Banners',
                component: reviewBanners
            },{
                path: 'offers-and-terms',
                name: 'Offers and Terms',
                component: reviewOffers
            }
        ]
    }
];

const router = new vueRouter({routes, linkActiveClass : "active"})

export default router
import Vue from 'vue'
import VueRouter from 'vue-router'
import BootstrapVue from 'bootstrap-vue'
import VueClipboard from 'vue-clipboard2'
import Axios from 'axios'

import store from './store'
import router from './router'
import app from './app.vue'

import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'
import 'font-awesome/css/font-awesome.css'

Vue.use(BootstrapVue)

Vue.use(VueRouter)

Vue.use(VueClipboard)

var vueApp = new Vue({
    el: '#vue-app',
    store: store,
    components: {
        app
    },
    router: router,
    template: `
        <div class="app">
          <app></app>
        </div>
    `
})
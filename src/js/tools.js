import _ from 'lodash'
import * as Cookies from "js-cookie"

const tools = {
    bind: function (key) {
        return {
            get () {
                return _.get(this.$store.state, key)
            },
            set (val) {
                this.$store.commit('set', {[key]: val})
            }
        }
    },
    cookieBind: function (key) {
        return {
            get () {
                return _.get(this.$store.state, key)
            },
            set (val) {
                this.$store.commit('set', {[key]: val})
                Cookies.set('selectedClient', val)
            }
        }
    },
    metaBind: function (key) {
        return {
            get () {
                return _.get(this.$store.state, key.call(this))
            },
            set (val) {
                this.$store.commit('set', {[key.call(this)]: val})
            }
        }
    }
}

export default tools
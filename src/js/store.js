import Vue from 'vue'
import Vuex from 'vuex'
import _ from 'lodash'
import * as Cookies from "js-cookie"

Vue.use(Vuex)

let defaultState = {
    baseUrl: 'http://dino.zionandzion.com',
    selectedClient: (Cookies.get('selectedClient')) ? Cookies.get('selectedClient') : ''
}

const store = new Vuex.Store({
    state: _.merge(defaultState, serverValues),
    getters: {
        getClientResponse: (state) => {
            return state.clientResponses[state.selectedClient]
        },
        getClientImageSections: (state) => {
            return state.clientImageSections[state.selectedClient]
        },
        getClientResponse: (state) => {
            return state.clientResponse[state.selectedClient]
        }
    },
    mutations: {
        set (state, payload) {
            Object.entries(payload).forEach(function ([key, val]) {
                _.set(state, key, val)
            })
        }
    },
    actions: {

    }
})

export default store
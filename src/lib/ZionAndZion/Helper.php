<?php
namespace ZionAndZion;

use Slim\Http\UploadedFile;

class Helper{

  /**
   * Helper constructor.
   */
  public function __construct(){

  }

  /**
   * Converts kebabcase string and makes it label ready
   * @param $string
   * @return string
   */
  static function labelify($string){
    $phrase = ucwords(str_replace('-', ' ', $string));
    $phrase = str_replace('Ars', 'ARS -', $phrase);
    return $phrase;
  }

  /**
   * Converts a CSV data into PHP array
   * @param $path - Path to csv file that should be constructed as array.
   * @param $offers - Boolean, if true return CSV data as indexed rows instead of associative array
   * @return array
   */
  static function csvToData($path, $offers = FALSE){
    $sourceArray = array_map('str_getcsv', file($path));
    $returnArray = array();

    if ($offers === FALSE) {
      foreach($sourceArray as $row){
        $returnArray[$row[0]] = $row[1];
      }
    } else {
      foreach($sourceArray as $row){
        $rowKey = '';
        foreach($row as $key => $value){
          // keyword column
          if ($key === 0){
            $rowKey = $value;
            $returnArray[$value] = '';
            // terms key
          } else if ($key === 1) {

          } else {
            $append = '';
            if ($key > 2){
              $append .= ',';
            }
            $append .= $value;
            $returnArray[$rowKey] .= $append;
          }
        }
      }
    }

    return $returnArray;
  }

  /**
   * Copies a directory recursively to a defined destination path
   * @param string $source_path
   * @param string $dest_path
   */
  static function copyDirRec(string $source_path, string $dest_path){
    $dir = opendir($source_path);
    mkdir($dest_path);
    while(false !== ( $file = readdir($dir)) ) {
      if (( $file != '.' ) && ( $file != '..' )) {
        if ( is_dir($source_path . '/' . $file) ) {
          self::copyDirRec($source_path . '/' . $file, $dest_path . '/' . $file);
        }
        else {
          copy($source_path . '/' . $file, $dest_path . '/' . $file);
        }
      }
    }
    closedir($dir);
  }


  /**
   * Deletes a directory recursively
   * @param string $path - Path of the directory that will be deleted
   */
  static function deleteDirRec(string $path){
    if (is_dir($path)) {
      $objects = scandir($path);
      foreach ($objects as $object) {
        if ($object != '.' && $object != '..') {
          if (is_dir($path . '/'. $object))
            self::deleteDirRec($path . '/' . $object);
          else
            unlink($path . '/' . $object);
        }
      }
      rmdir($path);
    }
  }

  /**
   * Moves the uploaded file to the upload directory and assigns it a unique name
   * to avoid overwriting an existing uploaded file.
   *
   * @param string $directory directory to which the file is moved
   * @param UploadedFile $uploaded file uploaded file to move
   * @return string filename of moved file
   */
  static function moveUploadedFile($directory, UploadedFile $uploadedFile){
    $filename = $uploadedFile->getClientFilename();
    $uploadedFile->moveTo($directory . DIRECTORY_SEPARATOR . $filename);
    return $filename;
  }
}
<?php

namespace ZionAndZion;

class Ngram
{

  /**
   * Helper constructor.
   */
  public function __construct()
  {

  }

  /**
   * Takes a word or phrase and returns an array of all patterns found in the string
   * @param $phrase - string of the phrase you want to receive ngram results for
   * @param $n - the letter count you want to use for pattern matching
   * @return array
   */
  private function getNgrams($phrase, $n = 3)
  {
    $ngrams = array();
    $len = strlen($phrase);
    for ($i = 0; $i < $len; $i++) {
      if ($i > ($n - 2)) {
        $ng = '';
        for ($j = $n - 1; $j >= 0; $j--) {
          $ng .= $phrase[$i - $j];
        }
        $ngrams[] = $ng;
      }
    }
    return $ngrams;
  }

  public function getBestMatch($phrase, $options)
  {
    $wholeWords = explode(' ', $phrase);
    $bigramWeight = 2;
    $trigramWeight = 6;
    $quadgramWeight = 12;
    $phraseBigrams = $phraseTrigrams = $phraseQuadgrams = array();

    foreach ($wholeWords as $word) {
      $phraseBigrams = array_unique(array_merge($phraseBigrams, $this->getNgrams($word, 2)));
      $phraseTrigrams = array_unique(array_merge($phraseTrigrams, $this->getNgrams($word, 3)));
      $phraseQuadgrams = array_unique(array_merge($phraseQuadgrams, $this->getNgrams($word, 4)));
    }

    $bestMatchNum = 0;
    $bestMatchString = '';

    foreach ($options as $option) {
      $wholeWords = explode(' ', $option);
      $optionBigrams = $optionTrigrams = $optionQuadgrams = array();

      foreach ($wholeWords as $word) {
        $optionBigrams = array_unique(array_merge($optionBigrams, $this->getNgrams($word, 2)));
        $optionTrigrams = array_unique(array_merge($optionTrigrams, $this->getNgrams($word, 3)));
        $optionQuadgrams = array_unique(array_merge($optionQuadgrams, $this->getNgrams($word, 4)));
      }

      $count = count(array_intersect($phraseBigrams, $optionBigrams)) * $bigramWeight;
      $count += count(array_intersect($phraseTrigrams, $optionTrigrams)) * $trigramWeight;
      $count += count(array_intersect($phraseQuadgrams, $optionQuadgrams)) * $quadgramWeight;

      if ($count > $bestMatchNum) {
        $bestMatchNum = $count;
        $bestMatchString = $option;
      }
    }

    if ($bestMatchNum > (strlen($bestMatchString) * 2)) {

      // uncomment to see nGram scoring
//        echo("   ".$bestMatchString." : ".$bestMatchNum."   ");

      return $bestMatchString;
    }
  }
}
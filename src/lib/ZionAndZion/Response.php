<?php

namespace ZionAndZion;

class Response{

  private $response_file;

  /**
   * Response constructor.
   * @param string $container_path - Direct path to the specific container.
   */
  public function __construct(string $container_path, string $environment){
    $this->response_file = $container_path . '/' . $environment . '/response.json';
  }

  /**
   * Appends data to the response.json object
   * @param string $key - key for the new object that is added to the response.json data
   * @param array $data - Associative array
   */
  public function addData(string $key, array $data){
    $response_data = json_decode(file_get_contents($this->response_file));
    $response_data->{$key} = $data;
    file_put_contents($this->response_file, json_encode($response_data, JSON_UNESCAPED_SLASHES));
  }

  /**
   * @param string $format - array|json - defaults to json
   * @return string
   */
  public function getData(string $format = 'json'){
    $data = file_get_contents($this->response_file);
    if($format == 'object'){
      return json_decode($data);
    } else if($format == 'array') {
      return get_object_vars(json_decode($data));
    } else {
      return $data;
    }
  }

}
<?php

namespace ZionAndZion;

use Symfony\Component\Yaml\Yaml;

/**
 * Requires ImageMagick PHP extension.
 */
class Offer{

  /**
   * Offer constructor.
   */
  public function __construct(){

  }

  /**
   * @param string $path - Path of directory where all images will be deleted from (not recursive).
   * @param array $file_type - File type extensions that should be deleted. I.e. ['jpg','png'].
   */
  public function deleteImages(string $path, array $file_type){

    foreach($file_type as $type){
      if(!isset($files)){
        $files = glob($path . '/*.' . $type);
      }else{
        $add = glob($path . '/*.' . $type);
        foreach($add as $a){
          $files[] = $a;
        }
      }
    }

    // Loop through final list of files and delete
    foreach($files as $file) {
      unlink($file);
    }
  }

  /**
   * Builds offer from supplied images and saves it to desired path.
   * Images are appended vertically in a descending order.
   * @param string $type - I.e. jpg or png.
   * @param string $path - The directory where output image should be saved.
   * @param string $fileName - String of the file name, excluding type extension.
   * @param array $sections - Array of paths for the building sections of the output image.
   */
  public function buildOffer(string $type, string $path, string $fileName, array $sections){
    if($path && $type && $fileName){

      if(!empty($sections)){

        if(!file_exists($path . $fileName . $type)) {
          $im = new \Imagick();
          foreach ($sections as $section) {
            $im->readImage($section);
          }
          $im->resetIterator();

          // Combine images
          $combined = $im->appendImages(true);
          $combined->setImageFormat($type);
          header('Content-Type: image/' . $type);
          file_put_contents($path . $fileName . '.' . $type, $combined);
        }else{
          trigger_error('Can\'t create an image that already exists.', E_USER_ERROR);
        }

      }else{
        trigger_error('Images array is required to build output image.', E_USER_ERROR);
      }
    }else{
      trigger_error('Name and type for the file are needed in order to build the image.', E_USER_ERROR);
    }
  }

  /**
   * Builds offers in development directory of specific container.
   * Sections are automatically pulled from /sections directory of specified container.
   * @param string $containerPath - Absolute path to container.
   * @param bool $returnKey - Returns the final key of the keyword offer relationship that is built after creating the new files
   * @return array
   */
  public function createOfferImages(string $containerPath, bool $returnKey){
    $devPath = $containerPath. '/development';

    $offers = Helper::csvToData($devPath . '/csv/offers.csv', true);
    $config = Yaml::parse(file_get_contents($containerPath . '/config.yml'));

    $offersDir = $devPath . '/images/offers';

    // Wipe image dir
    $this->deleteImages($offersDir, ['jpg']);

    // Build new images and save them with MD5 file names, also save array that maps what keywords belong to the newly created images
    $count = 0;
    $offerKey = [];
    $existingOffers = [];
    $offers['default'] = $config['defaults']['offer'];
//    array_push($offers, array('default', $config['defaults']['terms'], $config['defaults']['offer']));

    foreach($offers as $key => $value){
      // Use existing offer
      $exists = array_search($value, $existingOffers);
      if($exists) {
        // Add existing offer image to image key
        $offerKey[$key] = $offerKey[$exists];

        // Build offer
      }else{
        $imageList = explode(',', $value);
        if(count($imageList) === 0){
          $imageLst = array();
          foreach($value as $key => $val){
            if ($key > 1){
              array_push($imageList, $val);
            }
          }
          $imageList = array_values($imageList);
        }

        // Prepend dir path for sections
        foreach ($imageList as &$path) {
          $path = $offersDir . '/_sections/' . $path;
        }

        // Create image name
        $name = md5($containerPath . $count);

        // Build offer
        $offerFileType = 'jpg';
        $this->buildOffer($offerFileType, $offersDir . '/', $name, $imageList);

        // Append to existing offers and image key
        $existingOffers[$key] = $value;
        $offerKey[$key] = $name . '.' . $offerFileType;
      }

      $count++;
    }

    if ($returnKey){
      return $offerKey;
    }
  }
}
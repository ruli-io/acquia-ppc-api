<?php
use Symfony\Component\Yaml\Yaml;
use ZionAndZion\Helper;
use ZionAndZion\Offer;
use ZionAndZion\Response;

/**
 * Admin Route
 * /////////////
 */
$app->get('/v1/admin', function ($request, $response, $args) {
  if (in_array($_SERVER['REMOTE_ADDR'], IP_WHITELIST)) {

    $clientResponses = array();
    $clientImageSections = array();
    $clientResponseImages = array();

    $clients = glob(CONTAINERS_PATH . '/*' , GLOB_ONLYDIR);
    foreach($clients as $k => &$client){
      $folderName = end(explode('/', $client));

      if ($folderName === '_template'){
        unset($clients[$k]);
      }else{
        $client = array(
          'text' => Helper::labelify($folderName),
          'value' => $folderName
        );

        $clientPath = CONTAINERS_PATH . '/' . $folderName;
        $responseJson = file_get_contents($clientPath . '/production/response.json');
        $clientResponses[$folderName] = json_decode($responseJson);
        $sectionsFolder = $clientPath . '/production/images/offers/_sections';

        foreach (scandir($sectionsFolder) as $file) {
          if ('.' === $file) continue;
          if ('..' === $file) continue;

          $images[] = $file;
        }
        $clientImageSections[$folderName] = $images;

        $offersFolder = $clientPath . '/production/images/offers';

        foreach (scandir($offersFolder) as $file) {

          if ('.' === $file) continue;
          if ('..' === $file) continue;
          if ( $file === '_sections' ) continue;

          $builtImages[] = $file;
        }
        $clientResponseImages[$folderName] = $builtImages;

        //Get data for review and approve section
        $responseArray = (array) (json_decode($responseJson));
        $clientArr = array();
        //Get all types of data converted to array
        foreach ($responseArray as $arr => $data) {
          $clientArr[$arr] = (array) $data;
        }

        foreach ($clientArr as $type => $data) {

          if ($type == 'offers' || $type == 'banners'){
            foreach ($data as $key => $value) $data[$key] = basename($value);
            $clientArr[$type] = $data;
          }

          if ( $type != 'terms' ) {
            $tempArray = array();
            $res = array();

            $tempArray = array_flip($clientArr[$type]);

            foreach($tempArray as $k =>$v) $res[$k] = implode(", ", array_keys($clientArr[$type], $k));
            foreach($res as $k => $v) $res[$k] = explode(',',$res[$k]);
            $clientArr[$type]= $res;
          }

        }
        $clientArr['keywordTermTaxon'] = (array) Helper::csvToData($clientPath . '/production/csv/offers.csv');
        $clientResponse[$folderName] = $clientArr;
      }
    }

    array_unshift(
      $clients,
      array(
        'text' => 'Choose a client',
        'value' => null
      )
    );

    $context = array(
      "serverValues" => array(
        'clients' => array_values($clients),
        'clientResponses' => $clientResponses,
        'clientImageSections' => $clientImageSections,
        'clientResponse' => $clientResponse
      )
    );

    return $this->view->render($response, 'admin.phtml', $context);
  } else {
    return $response->withRedirect('/');
  }
})->setName('admin');


/**
 * CSV File Upload Route
 * /////////////
 */
$app->post('/v1/admin/{client}/upload', function ($request, $response, $args) {
  if (in_array($_SERVER['REMOTE_ADDR'], IP_WHITELIST)) {
    $csvDir = CONTAINERS_PATH . '/' . $args['client'] . '/production/csv' ;
    $uploadedFiles = $request->getUploadedFiles();
    $uploadedFile = $uploadedFiles['file'];
    echo('<pre>');
    print_r($uploadedFile);
    echo('</pre>');
    if ($uploadedFile->getError() === UPLOAD_ERR_OK) {
      $filename = Helper::moveUploadedFile($csvDir, $uploadedFile);
      $response->write('uploaded ' . $filename . '<br/>');
    }
  } else {
    return $response->withRedirect('/');
  }
})->setName('upload');


/**
 * Publish Route
 * /////////////
 */
$app->get('/v1/admin/{client}/publish', function ($request, $response, $args) {
  $body = '';
  $client = $args['client'];

  if (in_array($_SERVER['REMOTE_ADDR'], IP_WHITELIST)) {
    $body .= 'IP Confirmed<br />';
    $clientPath = CONTAINERS_PATH . '/' . $client;
    $config = Yaml::parse(file_get_contents($clientPath . '/config.yml'));

    $body .= 'Setting client disabled flag to true...<br />';
    $config['disabled'] = true;
    file_put_contents($clientPath . '/config.yml', Yaml::dump($config));


    $body .= 'Building offer images...<br />';
    $offer = new Offer();
    $newOffers = $offer->createOfferImages($clientPath, true);

    // Add pretty URL to images
    foreach ($newOffers as &$offer) {
      $offer = HOME_URL . '/images/offers/' . $client . '/' . $offer;
    }

    $environment = 'development';
    $r = new Response($clientPath, $environment);
    $body .= 'Adding offers to response...<br />';

    $r->addData('offers', $newOffers);

    $banners = Helper::csvToData($clientPath.'/'.$environment.'/csv/banners.csv');
    if (isset($banners)){
      $body .= 'Adding banners to response...<br />';
      $r->addData('banners', $banners);
    }

    $headlines = Helper::csvToData($clientPath.'/'.$environment.'/csv/headlines.csv');
    if (isset($headlines)){
      $body .= 'Adding headlines to response...<br />';
      $r->addData('headlines', $headlines);
    }

    $subheadlines = Helper::csvToData($clientPath.'/'.$environment.'/csv/subheadlines.csv');
    if (isset($subheadlines)){
      $body .= 'Adding subheadlines to response...<br />';
      $r->addData('subheadlines', $subheadlines);
    }

    $terms = Helper::csvToData($clientPath.'/'.$environment.'/csv/terms.csv');
    if (isset($terms)){
      $body .= 'Adding terms to response...<br />';
      $r->addData('terms', $terms);
    }

    $versionPath = $clientPath . '/version/' . date('Y-m-d_H:i:s');
    $body .= 'Creating new archive directory ('.$versionPath.')<br />';
    mkdir($versionPath);
    $body .= 'Copying production contents into archive directory...<br />';
    Helper::copyDirRec($clientPath . '/production', $versionPath);
    $body .= 'Deleting previous production folder contents...<br />';
    Helper::deleteDirRec($clientPath . '/production');
    $body .= 'Copying approved development folder contents into production directory...<br />';
    Helper::copyDirRec($clientPath . '/development', $clientPath . '/production');

    $body .= 'Setting client disabled flag back to false...<br />';
    $config['disabled'] = false;
    file_put_contents($clientPath . '/config.yml', Yaml::dump($config));

    $body .= 'NEW VERSION PRODUCTION READY';

    $response->getBody()->write($body);
    return $response;
  } else {
    return $response->withRedirect('/');
  }
});

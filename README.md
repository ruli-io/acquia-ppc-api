# Acquia Showcase - PPC/SEM Dynamic Content API
---
## Overview

- At some point I was managing 50+ micro-sites that were used for PPC/SEM. These all had many offers that would be served when conditions were met. I.e., if a user typed "A/C repair" in their search query, the micro-site would display an offer related to that query and also tailor the titles and content to the messaging.
- These solutions were getting hard and repetitive to manage, so we decided to build a service that could house offers for each available/new marketing site and could be accessed through an HTTP request. It was meant to organize this process and allow for updates in a centralized location. Also, this would be ready for the future case of creating omni-channel campaigns.
- We chose to use a practical PHP framework, Slim. This application was mainly an API, and Slim gives you the tools to easily create routes, authentication, requests, etc.

## Key Features

- /src/lib - Contains the controllers for the API. These were used throughout the codebase.
- /src/routes.php - Public facing routes that would deliver the custom content.
- /src/js - These Vue files were used for the admin portion of the application. We wanted to have an interactive experience with real-time feedback. We chose the Vue framework because it can be coupled with other technologies easily. In this case, having a PHP API and a Vue interface worked really well.
